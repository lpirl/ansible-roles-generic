This role provides some generic hardening (kernel, /proc, umask,
passwords, ssh, nginx, …).

You should really check this role's defaults before applying.
