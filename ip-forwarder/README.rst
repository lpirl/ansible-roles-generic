Enables IP forwarding; i.e., allows ip-forwarding at kernel level.
