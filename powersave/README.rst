A role for generic power saving.

* most settings are configured via "laptop-mode-tools"
* periodic tasks (e.g. cron, systemd timers) are prevented from being
  executed when on battery
* adds systemd targets ``on-battery.target`` and ``on-ac.target`` which
  reflect the current power supply; can be used to run units on
  ac/battery only; configuration via this role's variable
  ``units_disabled_on_battery_extra`` or manually, e.g.::

    $ systemctl edit my.service
    [Unit]
    After=on-ac.target
    # stop this unit when AC goes offline:
    BindsTo=on-ac.target

    [Install]
    # start this unit when AC goes online:
    WantedBy=on-ac.target

  Then remember to ``systemctl enable my.service`` so the ``[Install]``
  sections are getting re-processed.

This role somewhat conflicts with the role "performance". Please see the
README file of the role "performance" for notes on the use of both roles
simultaneously.
