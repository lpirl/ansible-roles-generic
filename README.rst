.. image:: https://gitlab.com/lpirl/ansible-roles-generic/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/ansible-roles-generic/pipelines
  :align: right


generic Ansible roles for Phoenixes and Snowflakes
==================================================

The roles in this repository provide general, largely application agnostic
configurations for hosts.
Although no applications to provide specific services etc. are installed,
there are of course different roles for different use cases (Do we need
a firewall? A mail transfer agent? Real-time capabilities? …).

They follow `conventions <conventions.rst>`__ in order to be applicable
to hosts which are managed solely via Infrastructure as Code or
configuration management systems ("Phoenix hosts", loosely speaking) *and*
hosts which also require to be configured manually, i.e., which have local
modifications of configuration files etc. ("Snowflake hosts", loosely
speaking).


static files
------------

For reference (and easier browsing), all static files of all roles in
this repository (``*/files/*``) can be found `on the pages of this
project <https://lpirl.gitlab.io/ansible-roles-generic/>`__.


----

History:

The roles in this repository are a rewrite of
`github.com/lpirl/ansible-roles
<https://github.com/lpirl/ansible-roles>`__
and also some scripts and configuration files previously kept in
`github.com/lpirl/admintools <https://github.com/lpirl/admintools/>`__
are now incorporated here.
