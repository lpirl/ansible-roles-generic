A role which configures some generic performance optimizations such as
IRQ balancing, NUMA balancing, avoiding thundering herd problems, etc.

This role somewhat conflicts with the role "powersave". If this role
and the role "powersave" are used simultaneously, consider setting an
appropriate tuned profile via this role's variables (e.g. "powersave"
or "balanced").
