#!/usr/bin/env python
"""
An example/template watchdog test/repair script written in Python.
"""
import argparse
from sys import exit as sys_exit
from logging import getLogger, DEBUG, debug, error
from subprocess import check_output



def test():
  output = check_output(('echo', 'echo test'),
                        universal_newlines=True)
  print(output.strip())

  if output.strip() == '':
    error('echo did not echo')
    sys_exit(1)



def repair():
  debug('start repair')
  print('repair')



def main():

  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
  )

  parser.add_argument('-d', '--debug', action='store_true', default=False,
                      help='turn on debug messages')
  parser.add_argument('action', choices=('test', 'repair'),
                      help='activate or deactivate')

  args = parser.parse_args()

  logger = getLogger()
  logger.name = ''
  if args.debug:
    logger.setLevel(DEBUG)

  if args.action == 'test':
    test()
  elif args.action == 'repair':
    repair()
  else:
    assert False, 'reached code which should not be reachable'



if __name__ == '__main__':
  main()
