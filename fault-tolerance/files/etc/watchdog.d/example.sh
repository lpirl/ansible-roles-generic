#!/bin/sh

if [ "$1" = "test" ]
then
  echo "test"

elif [ "$1" = "repair" ]
then
  echo "repair"

else
  echo "please specify either 'test' or 'repair' as first argument"
fi
