A role to setup some generic fault-tolerance mechanisms to cope with
faults at the level of the OS (e.g., panics) and hardware (e.g.,
overheating).
