This document summarizes conventions used in this repository.

The conventions aim to

* achieve consistent sources of the roles in this repository,
* achieve consistent ways of how configurations are applied to the
  managed hosts, and
* facilitate fully-automated management of hosts (i.e., solely via
  Infrastructure as Code etc.)

– obviously.
Additionally, the conventions – maybe less usual – aim to

* allow manual changes to configurations on the managed hosts,
  hence
* avoid automated configuration overwriting manual changes on the
  managed hosts,
* make configurations applied through automation transparent on the
  managed systems (e.g., by marking such configurations with
  comments).

Contents:

.. contents:: \

role sources
============

This section notes how the "source code" of the roles is organized.

configuration variables
-----------------------

This section notes which variables are provided usually, how the
variables are named, etc.

dynamic defaults
................

Dynamic default variables are variables which are not defined with
static values. Instead, they are derived from other variables' values
(e.g., if the host is virtual or not). The dynamic values are intended
to provide useful defaults for a wider range of possible configurations
as it would be possible with just static values.

Values of dynamic default variables which are based on other variables
(whether provided by Ansible by default or set by the role
``custom-facts``) are defined/calculates in the roles'
``defaults/main.yml``. If defaults require the output of a command
etc., they are calculated directly in the conditionals of tasks or as a
separate ``set_fact`` task of the role.

.. _extra:

``<variable name>_extra``
.........................

The roles (mostly) provide ``<variable name>_extra`` which is an
addition to ``<variable name>`` (e.g., lists are extended, dictionaries
are merged, etc.). PRs welcome where ``<variable name>_extra`` is not
implemented yet.

Alternatively, you can use the Ansible configuration
``hash_behaviour = merge`` to achieve merging of dictionaries
"globally", e.g., where ``<variable name>_extra`` is not implemented yet.

``set_…``/``use_…``
...................

We also try to consistently use Boolean ``set_…`` variables to
enable/disable configurations where no additional tool needs to be
installed (e.g., kernel parameters) and ``use_…`` to enable/disable
installations and configurations of additional tools (e.g., a watchdog).

per-role tags
-------------

The roles' tasks are tagged with ``role_<role name>``, which enables
running/preventing to run all role's tasks from command line without
having to edit your Playbook.

where to store configurations
-----------------------------

This section notes where in the roles' files the configurations for the
managed hosts are stored.

It is preferred to store configurations in the roles' default variables.
The more individual/"non-generic" the configurations are, the
easier it is for overriding etc. when the configurations are stored in
variables (instead of, e.g., files). To enable the aforementioned
overriding of configurations stored in variables, dictionaries need to be preferred (see `<variable name>_extra <extra_>`__).

Larger, more static/generic configurations might of course be stored in
files in the ``files`` directory of the corresponding role. Storing
configurations in files might also ease re-use by others.
Hence, storing configurations in files is especially meaningful, when
the file contents make sense on their (i.e., the configuration files
are usable on their own). For example, a complete shell configuration
would be a usable file on its own, whereas a block of text which is to
be added to another configuration file is not.

If possible, the files in the roles' ``files`` directory should be placed
in directories mimicking the directory structure on the managed host.
For example, a configuration for the ssh server will remotely go into
``/etc/ssh/sshd_config`` and should locally be stored in
``<role name>/files/etc/ssh/sshd_config``. This makes the destination of
the configurations in the ``files`` directory transparent locally.

If configurations are (more or less) invariant considering the purpose
and flexibility of a role, they can also be hard-coded in the roles'
tasks.

privilege escalation
--------------------

Specify ``become: yes`` in the tasks and don't require users of the
roles to specify ``become: yes`` globally on Play level. To use
``become: yes`` for all tasks in a role (opt out), include a
``_main.yml`` file in ``main.yml`` with ``become: yes``.

Example ``<role name>/tasks/main.yml``::

  - name: become privileged user by default and tag all tasks
    import_tasks: _main.yml
    become: yes
    tags: role_<role name>

YAML syntax
-----------

YAML "inline" syntax is to be avoided, e.g.::

  # NO:
  - package: name=[foo, bar] state=present

  # YES:
  - package:
    name:
      - foo
      - bar
    state: present

configurations on managed hosts
===============================

Notes on conventions regarding how to modify configurations on remote
hosts.

modifying configuration files
-----------------------------

If possible, the ``blockinfile`` module is used to put configurations in
place on the managed hosts. That way, the configurations will
automatically be surrounded by "ANSIBLE MANAGED BLOCK" comments, which
makes the automated configuration transparent and allows manual
additions on the managed hosts.

If the configurations are stored in a file in the ``files`` directory
of the role, the ``file`` lookup is used to retrieve the file contents
for the ``blockinfile`` module. To avoid the need to note a file's path
two times in the task (as source and as destination), ``with_items``
can be used.

Complete example::

  - name: add apt repository for foo
    blockinfile:
      path: "/{{ item }}"
      state: present
      create: yes
      mode: u+x
      block: "{{ lookup('file', item) }}"
    with_items:
      - etc/apt/sources.list.d/foo.list

Or, if all files in a local directory are to be transferred::

  - name: set resource limits for users and groups
    blockinfile:
      path: "/etc/security/limits.d/{{ item.path }}"
      block: "{{ lookup('file', item.src) }}"
      create: yes
    with_filetree: etc/security/limits.d/
    loop_control:
      label: "{{ item.path }}"

If possible, the configurations should be placed in a separate file
(e.g., in directories like ``/etc/cron.d``) on the managed hosts.
Also for such separate files, using ``blockinfile`` is preferred or,
at least, the file on the managed hosts should be named in a way that
makes it transparent that the file is managed by the automated
configuration.

If neither ``blockinfile`` nor a separate configuration file can be
used, the ``lineinfile`` module is preferred. While this does not make
it transparent on the managed hosts which lines were set by the
automated configuration, it still allows for manual modifications of
configurations on the managed hosts, at least.
