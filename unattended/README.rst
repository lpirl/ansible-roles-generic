This role makes remote hosts upgrade unattended.

Currently, this is implemented by making the Play this role is
referenced in available on the remote (see role ``remote-playable``)
and schedule its regular execution.
