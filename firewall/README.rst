This role sets up a firewall on OSI layer 4 (iptables) and 7 (fail2ban).

Note that to avoid making the target host inaccessible, the SSH port
must be provided explicitly with the variable ``firewall_ssh_port``.

A set of generic rules for iptables is provided. Individual rules, incl.
NAT rules etc., can be provided via the role's variables.
The fail2ban jails are – as configured by this role's default variables
– enabled if the corresponding command is available (e.g. Apache).
