#!/usr/bin/env python3
"""
Prints df-like information about mounted files systems which have less
free space than a given percentage.
"""

import argparse
from subprocess import check_output

def main():

  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
  )

  parser.add_argument('min_free_percent', type=int,
                      help='minimum free disk space, in percent')

  args = parser.parse_args()
  min_free_percent = args.min_free_percent
  max_used_percent = 100 - min_free_percent

  mountpoints = []

  # filter mount points first, so network shares etc. are not queried
  with open('/proc/mounts') as mounts_fp:
    for line in mounts_fp:
      if not line.startswith('/dev/'):
        continue
      if line.startswith('/dev/loop'):
        continue

      line = line.split()[1]

      # unescape (see ``man fstab``, ctrl + f "escape")
      line = line.replace('\\040', ' ')
      line = line.replace('\\011', '\t')

      mountpoints.append(line)

  df_lines = check_output(
    ['df', '-Ph'] + mountpoints, text=True
  ).splitlines()

  print_lines = []
  for line_numer, line in enumerate(df_lines):

    if line_numer == 0:
      continue

    percent = line.split(maxsplit=5)[4]

    if int(percent.rstrip('%')) > max_used_percent:
      print_lines.append(line)

  if print_lines:
    print(f'The following file systems have less than '
          f'{min_free_percent}% free disk space:')
    print('')
    print(f'  {df_lines[0]}', end='\n  ')
    print('\n  '.join(print_lines))
    print('')

if __name__ == '__main__':
  main()
